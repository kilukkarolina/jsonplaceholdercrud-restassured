import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonplaceholderGETTest {

    private final String BASE_URL = "https://jsonplaceholder.typicode.com";
    private final String PHOTOS = "photos";
    private final String USERS = "users";

    @Test
    public void jsonplaceholderReadAllPhotos(){

        Response response = given()
                .when()
                .get(BASE_URL + "/" + PHOTOS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        List<Integer> albums = json.getList("albumId");
//        albums.stream()
//                .forEach(System.out::println);
        assertEquals(5000, albums.size());

    }

    @Test
    public void jsonplaceholderReadOnePhoto(){

        Response response = given()
                .pathParam("photoId", 1)
                .when()
                .get(BASE_URL + "/" + PHOTOS + "/{photoId}")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertEquals(Integer.valueOf(1), json.get("albumId"));
        assertEquals("accusamus beatae ad facilis cum similique qui sunt", json.get("title"));
        assertEquals("https://via.placeholder.com/600/92c952", json.get("url"));
        assertEquals("https://via.placeholder.com/150/92c952", json.get("thumbnailUrl"));

    }

    @Test
    public void jsonplaceholderReadAllUsersWithCheckingEmail(){

        Response response = given()
                .when()
                .get(BASE_URL + "/" + USERS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        List<String> emails = json.getList("email");
        System.out.println(" ");
        System.out.println("Emaile wszystkich użytkowników: ");
        emails.forEach(System.out::println);

        boolean anyMatchEmailEndsWithPl = emails.stream()
                .anyMatch(email -> email.endsWith(".pl"));

        System.out.println(" ");
        if (!anyMatchEmailEndsWithPl)
            System.out.println("Żaden email nie kończy się na '.pl'.");
        else System.out.println("Istnieją emaile kończące się na '.pl'.");
    }

}
