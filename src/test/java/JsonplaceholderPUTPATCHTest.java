import com.github.javafaker.Faker;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonplaceholderPUTPATCHTest {

    private final String BASE_URL = "https://jsonplaceholder.typicode.com";
    private final String PHOTOS = "photos";

    private static Faker faker;
    private Integer fakeAlbumId;
    private String fakeTitle;

    @BeforeAll
    public static void beforeAll(){
        faker = new Faker();
    }

    @BeforeEach
    public void beforeEach(){
        fakeAlbumId = faker.number().numberBetween(1,1000);
        fakeTitle = faker.name().title();
    }

    @Test
    public void jsonplaceholderUpdatePhotoPUTTest(){

        JSONObject photo = new JSONObject();
        photo.put("albumId", fakeAlbumId);
        photo.put("title", fakeTitle);
        photo.put("url", "https://via.placeholder.com/600/92c952");
        photo.put("thumbnailUrl", "https://via.placeholder.com/150/92c952");

        Response response = given()
                .pathParam("photoId", 1)
                .contentType("application/json")
                .body(photo.toString())
                .when()
                .put(BASE_URL + "/" + PHOTOS + "/{photoId}")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertEquals(Integer.valueOf(1), json.get("id"));
        assertEquals(fakeAlbumId, json.get("albumId"));
        assertEquals(fakeTitle, json.get("title"));
    }

    @Test
    public void jsonplaceholderUpdatePhotoPATCHTest(){

        JSONObject photo = new JSONObject();
        photo.put("title", fakeTitle);

        Response response = given()
                .pathParam("photoId", 1)
                .contentType("application/json")
                .body(photo.toString())
                .when()
                .put(BASE_URL + "/" + PHOTOS + "/{photoId}")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertEquals(Integer.valueOf(1), json.get("id"));
        assertEquals(fakeTitle, json.get("title"));
    }
}
